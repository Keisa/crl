const mongoose = require('mongoose');

const	Emergency = mongoose.model('Emergency', {
	email:		{ type: String, required: true },
	messages:	[{
		userType:	 	{ type: String, required: true },
		target:			{ type: String, required: true },
		threatType:		{ type: String, required: true },
		phoneNumber:	{ type: String, required: true }
	}]
}, 'emergency');

module.exports = Emergency;
