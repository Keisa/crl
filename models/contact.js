const	mongoose = require('mongoose');

const	Contact  = mongoose.model('Contact', {
	fname:		{ type: String, required: true },
	lname:		{ type: String, required: true },
	email:		{ type: String, required: true },
	role:		{ type: String, required: true },
	messages:	[{
		subject:	{ type: String, required: true },
		date:		{ type: String, required: true },
		status:		{ type: String, required: true }, // open, close
		replies: 	[{
			from:	{ type: String },
			to:		{ type: String },
			text:	{ type: String },
			date:	{ type: String }
		}]
	}]
}, 'contacts');

module.exports = Contact;
