const	emailRegex = /[a-zA-Z0-9_\.-]{1,30}@[a-zA-Z0-9_]{1,10}\.[a-zA-Z]{1,4}/;
const	nameRegex = /[a-zA-Z_]{3,13}/
const	MIN_LENGTH = 10;
const	MAX_LENGTH = 2000;
const	roles = [
	'Platform user',
	'Software developer',
	'Engeneer exec/manager',
	'Product Managment',
	'Marketing',
	'Security and Compilance',
	'Other'
];

let		validate = {
	email: function (value) {
		if (!value)
			return (false);
		return (emailRegex.test(value));
	},
	name: function (value) {
		if (!value)
			return (false);
		return (nameRegex.test(value));
	},
	text: function (value) {
		if (!value)
			return (false);
		return (value.length >= MIN_LENGTH && value.length <= MAX_LENGTH);
	},
	role: function (value) {
		if (!value)
			return (false);
		value = roles.find(function (curValue) {
			return (curValue == value);
		});
		return (value != undefined);
	},
	validateAll: function (objs) {
		let	err = {};

		for (let obj in objs) {
			obj = objs[obj];
			if (!this[obj.type](obj.value))
				return ({isError: true, message: obj.name + ' is invalid.'});
		}
		return ({isError: false, message: ''});
	}
};

module.exports = validate;

/*
// -- email validation test
console.log(validate.email('john@doe.me'));
console.log(validate.email('johndoe.me'));
console.log(validate.email('john@doeme'));
console.log(validate.email(''));
console.log(validate.email('john_doe@gmail.com'));
console.log(validate.text("Don't panic this is just a test text !!"));
*/
