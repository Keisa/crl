'use strict';
const	mongoose = require('mongoose');
const	validate = require('./validate');
const	Contact = require('../contact');
const	Emergency = require('../emergency');
const	lib = require('./lib');

const	roles = [
	'Platform contact',
	'Software developer',
	'Engeneer exec/manager',
	'Product Managment',
	'Marketing',
	'Security and Compilance',
	'Other'
];

mongoose.connect('mongodb://localhost/mydb', { useNewUrlParser: true, connectTimeoutMS: 3000, useFindAndModify: false });

let contactDb = {
	all: function (callback, model) {
		if (model === undefined)
			model = Contact;
		model.find({}, function (err, contacts) {
			if (lib.isFunction(callback))
				callback(err, contacts);
		});
	},
	find: function (query, callback, model) {
		if (model === undefined)
			model = Contact;
		model.findOne(query, {}, function (err, contact) {
			if (lib.isFunction(callback))
				callback(err, contact);
		});
	},
	insertContact: function (contactData, callback) {
		let	curDate = new Date();
		let	messageObj;
		let	contact;

		contactData.date = curDate.toString();
		contact = lib.iniContact(contactData);
		contact.save(function (err) {
			if (lib.isFunction(callback)) {
				if (err)
					callback({ isError: true, message: err.message });
				else
					callback({ isError: false, message: '' });
			}
		});
	},
	insertMessage: function (contactData, callback) {
		let	$this = this;
		let	message = null;
		let	curDate = new Date();
		let	messageIndex = -1;

		this.find({ 'email': contactData.email }, function (err, contact) {
			if (err) {
				if (lib.isFunction(callback))
					callback({ isError: true, message: err.message });
			}
			else if (contact === null) {
				$this.insertContact(contactData, callback);
			}
			else {
				message = contact.messages.find(function (curValue) {
					messageIndex++;
					return (curValue.subject === contactData.subject);
				});
				if (message === undefined) {
					message = {
						'subject': contactData.subject,
						'date': curDate.toString(),
						'status': 'open',
						'replies': [{
							'text': contactData.text,
							'from': contactData.from,
							'to': contactData.to,
							'date': curDate.toString()
						}]
					}
					Contact.updateOne({ '_id': contact._id }, { $push: { 'messages': message } }, function (err) {
						let	errObj = {
							isError: err ? true : false,
							message: err ? err.message : ''
						}
						if (lib.isFunction(callback))
							callback(errObj);
					});
				}
				else {
					message = {
						'text': contactData.text,
						'from': contactData.from,
						'to': contactData.to,
						'date': curDate.toString()
					};
					messageIndex = 'messages.' + messageIndex + '.replies';
					let toPush = {};
					toPush[messageIndex] = message;
					Contact.updateOne({ 'email': contactData.email }, { $push: toPush }, function (err) {
						let	errObj = {
							isError: (err ? true : false),
							message: (err ? err.message : '')
						}
						if (lib.isFunction(callback))
							callback(errObj);
					});
				}
			}
		})
	},
	insertEmergency: function (recordData, callback) {		
		let	curDate = new Date();
		let	record;

		record = lib.iniEmergency(recordData);
		record.save(function (err) {
			if (lib.isFunction(callback)) {
				if (err)
					callback({ isError: true, message: err.message });
				else
					callback({ isError: false, message: '' });
			}
		});
	},
	insertEmergencyMessage: function (recordData, callback) {
		let	$this = this;
		let	message = null;
		let	toPush = null;

		this.find({ 'email': recordData.email }, function (err, record) {
			if (err) {
				if (lib.isFunction(callback))
					callback({ isError: true, message: err.message });
			}
			else if (record === null) {
				$this.insertEmergency(recordData, callback);
			}
			else {
				toPush = {
					'messages': {
						'contactType': recordData.contactType,
						'target': recordData.target,
						'threatType': recordData.threatType,
						'phoneNumber': recordData.phoneNumber
					}
				};
				Emergency.updateOne({ email: record.email }, { $push: toPush }, function (err) {
					if (err)
						callback({ isError: true, message: err.message });
					else
						callback({ isError: false, message: '' });
				});
			}
		}, Emergency);
	}
};

module.exports = {
	'contactDb': contactDb
};
