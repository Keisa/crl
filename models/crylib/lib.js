'use strict';
const Contact = require('../contact');
const Emergency = require('../emergency');

const	lib = {
	isFunction: function (obj) {
		return (typeof obj === 'function');
	},
	iniContact: function (data) {
		let	obj = new Contact({
			'fname': data.fname,
			'lname': data.lname,
			'email': data.email,
			'role': data.role,
			'messages': [{
				'subject': data.subject,
				'date': data.date,
				'status': data.status,
				'replies': [{
					'text': data.text,
					'from': data.from,
					'to': data.to,
					'date': data.date
				}]
			}]
		});
		return (obj);
	},
	iniEmergency: function (data) {
		let	obj = Emergency({
			'email': data.email,
			'messages': [{
				'userType': data.userType,
				'target': data.target,
				'threatType': data.threatType,
				'phoneNumber': data.phoneNumber
			}]
		});
		return (obj);
	}
};

module.exports = lib;
