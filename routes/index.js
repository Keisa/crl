const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth');

//router.get('/', express.static(path.join(__dirname, 'public')))
// Dashboard
router.get('/dashboard', ensureAuthenticated, (req, res) =>
  res.render('dashboard', {
    user: req.user
  })
);

module.exports = router;
