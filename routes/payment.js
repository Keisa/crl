var paypal = require('paypal-rest-sdk');

paypal.configure({
  'mode': 'sandbox', // PP sandbox
  'client_id': 'YOUR_CLIENT_ID_HERE', 
  'client_secret': 'YOUR_CLIENT_SECRET_HERE' 
});


app.use('/order', express.static(path.join(__dirname, 'public')));

app.get('/order' , (req , res) => {
    res.redirect('/buy.html'); 
})

// start payment process 
app.get('/buy' , (req, res) => {
	// create payment object 
    var payment = {
            "intent": "authorize",
	"payer": {
		"payment_method": "paypal"
	},
	"redirect_urls": {
		"return_url": "http://138.197.190.12:1337/success",
		"cancel_url": "http://138.197.190.12:1337/error"
	},
	"transactions": [{
		"amount": {
			"total": 9.00,
			"currency": "USD"
		},
		"description": "Standard Plan"
	}]
    }


	// call the create Pay method 
    createPay(payment) 
        .then((transaction) => {
            var id = transaction.id; 
            var links = transaction.links;
            var counter = links.length; 
            while (counter --) {
                if (links[counter].method == 'REDIRECT') {
					// Redirect to paypal where user approves the transaction.
                    return res.redirect(links[counter].href)
                }
            }
        })
        .catch( ( err ) => { 
            console.log( err ); 
            res.redirect('/error');
        });
}); 


app.get('/success' , (req ,res ) => {
    console.log(req.query); 
    res.redirect('/success.html'); 
})

app.get('/error' , (req , res) => {
    console.log(req.query); 
    res.redirect('/error.html'); 
})

var createPay = (payment) => {
    return new Promise((resolve, reject) => {
        paypal.payment.create(payment, function(err, payment) {
         if (err) {
             reject(err); 
         }
        else {
            resolve(payment); 
        }
        }); 
    });
}
