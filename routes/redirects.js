const express = require('express');
const morgan = require('morgan');
const favicon = require('serve-favicon');
const bcrypt = require('bcryptjs');
const passport = require('passport');
const path = require('path');
const fs = require('fs');
const request = require('request');

const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');
const hpp = require('hpp');
const lib = require('../models/crylib/lib');
const contactCalls = require('../models/crylib/contactcalls');

const winston = require('../config/winston');

module.exports = function(app, basepath) {

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use(hpp());

app.use(morgan('combined', { stream: winston.stream }));

app.use(favicon(path.join('public/images', 'favicon.ico')));

app.use('/', express.static((__dirname, 'public')));

/* set ejs module config */
app.set('view engine', 'ejs');

// change the default views directory to 'pages'
app.set('views', path.join(basepath, 'pages'));
app.use(cookieParser());

contactCalls.forEach(function (route) {
	if (lib.isFunction(route.next) === false)
		route.next = function () {}
	switch (route.method) {
		case 'get': {
			app.get(route.url, route.call, route.next);
		} break ;
		case 'post': {
			app.post(route.url, route.call, route.next);
		} break ;
	}
});

app.get('/dash', function (req, res) {
	res.render('dashboard');
});

app.get('/services', function (req, res) {
	res.sendFile(basepath + '/pages/services.html');
});

app.get('/', function(req, res) {
	res.sendFile(basepath + '/pages/');
});

app.get('/login', function(req, res) {
	res.sendFile(basepath + '/pages/login.html')
});

app.get('/about/Rei', function(req, res) {
	res.sendFile(basepath + '/pages/intro/index.html')
});

app.get('/forgot', function(req, res) {
	res.sendFile(basepath + '/pages/forgot.html')
});

app.get('/team', function(req, res) {
	res.sendFile(basepath + '/pages/team.html')
});

app.get('/training', function(req, res) {
	res.sendFile(basepath + '/pages/training.html')
});

app.get('/pricing', function(req, res) {
	res.sendFile(basepath + '/pages/pricing.html')
});

app.get('/order', function(req, res) {
	res.sendFile(basepath + '/pages/order.html')
});

app.get('/legal/privacy-policy', function(req, res) {
	res.sendFile(basepath + '/pages/privacy.html')
});

app.get('/legal/terms-of-service', function(req, res) {
	res.sendFile(basepath + '/pages/termsofservice.html')
});

app.get('/register', function(req, res) {
	res.sendFile(basepath + '/pages/register.html'), {
		user: req.user
	};
});

app.use((req, res, next) => res.status(404).sendFile('404.html', { root: './pages/error/' }));
app.use((req, res, next) => res.status(500).sendFile('500.html', { root: './pages/error/' }));

// Handling reCaptcha

app.post('/register/captcha', function(req, res) {
 	if (req.body === undefined || req.body === '' || req.body === null) {
		return res.json({"responseError" : "captcha error"});
	}
	const secretKey = "6LdmXIYUAAAAAKY0Veffr2Yv-tOjXj8heDuteR_5";
	const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body + "&remoteip=" + req.connection.remoteAddress;
	request(verificationURL, function(error, response, body) {
	body = JSON.parse(body);
	if (body.success !== undefined && !body.success) {
		return res.json({"responseError" : "Failed captcha verification."});
	}
	res.json({"responseSuccess" : "Sucess"});
	});
});

// Register
app.post('/register', (req, res) => {
  const { name, email, password, password2 } = req.body;
  let errors = [];

  if (!name || !email || !password || !password2) {
    errors.push({ msg: 'Please enter all fields' });
  }

  if (password != password2) {
    errors.push({ msg: 'Passwords do not match' });
  }

  if (password.length < 6) {
    errors.push({ msg: 'Password must be at least 6 characters' });
  }

  if (errors.length > 0) {
    res.render('register', {
      errors,
      name,
      email,
      password,
      password2
    });
  } else {
    User.findOne({ email: email }).then(user => {
      if (user) {
        errors.push({ msg: 'Email already exists.' });
        res.render('register', {
          errors,
          name,
          email,
          password,
          password2
        });
      } else {
        const newUser = new User({
          name,
          email,
          password
        });

        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then(user => {
                req.flash(
                  'success_msg',
                  'You have registered successfully! Login to view your dashboard.'
                );
                res.redirect('/login');
              })
              .catch(err => console.log(err));
          });
        });
      }
    });
  }
});

// Login
app.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/users/login',
    failureFlash: true
  })(req, res, next);
});

// Logout
app.get('/logout', (req, res) => {
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/users/login');
});

}
