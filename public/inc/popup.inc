		<center>
		<div class="message-close" onclick="popup('ghelp')"></div>
		<div class="message-illustration"></div>
		<div class="message" id="needhelppopup">
			<div class="topwhite">
			</br>
			<h1>Get instant help from our professionals</h1>
				<div class="row">
					<label id="leftradio" class="radio-btn col-6">
						<input type="radio" checked="checked" name="radio" value="individual"></input>
						<span class="checkmark"><span>Individual</span></span>
					</label>
					<label id="rightradio" class="radio-btn col-6">
						<input type="radio" checked="checked" name="radio" value="business"></input>
						<span class="checkmark"><span>Business</span></span>
					</label>
				</div>
			</div>
			</br>
			<form enctype="application/json" method="post" style="display:block;" id="popup-individual">
				</br>
						<input type="text" placeholder="E-mail*" name="email" required></input>
						<input type="text" placeholder="Company*" name="target" required></input>
						<input type="text" placeholder="Threat type*" name="threatType" required></input>
						<input type="hidden" value="business" name="userType" required></input>
						<input type="tel" placeholder="Phone number*" id="telephone" name="phoneNumber" required></input>
				<input type="submit" class="btn btn-ok btn-square" value="GET HELP"></button>
			</form>
		</div>
		</center>
