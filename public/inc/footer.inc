		<div class="footer-sections">
			<div class="footer-section1">
				<ul>
					<li class="footer-title" style="margin-bottom: 10px;">Navigate the website</li>
					<li><a>here</a></li>
				</ul>
			</div>
		<div class="footer-section2">
			<ul>
				<li class="footer-title" style="margin-bottom: 10px;">Our services</li>
				<li>Web app scanning</li>
				<li>Network Penetration testing</li>
			</ul>
		</div>
		<div class="footer-section3">
			<ul>
				<li class="footer-title" style="margin-bottom: 10px;">More about us</li>
				<li><a href="/team">Our Team</a></li>
				<li><a href="/legal/terms-of-service">Terms and conditions</a></li>
				<li><a href="/legal/privacy-policy">Privacy Policy</a></li>
			</ul>
		</div>
		</div>
		<p class="footer-copyright"><a href="/legal/privacy-policy">&copy; 2018 Crynel team</a></br></br></p>
