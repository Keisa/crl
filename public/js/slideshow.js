var images = ["back.jpg","imagetest2.png","imagetest3.jpg"];
var comments = ["Future is loading...", "The future of cyber security is now", "security is THE priority"];
var container  = document.getElementById("slide-container");
var container_text = document.getElementById("slide-text");
var image_index = 0;

if (container)
{
	slide()
		setInterval(slide, 3000);

	function slide()
	{
		if (image_index >= images.length)
			image_index = 0;
		$("#slide-text").animate({opacity: 0});
		container.style.backgroundImage = "url(" + "/images/" + images[image_index] + ")";
		container_text.innerHTML = comments[image_index];
		$("#slide-text").animate({opacity: 1});
		image_index++;
	}
}
