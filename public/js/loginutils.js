var showerrorstatus = 0;
var verificationtoggle = 0;

function usernameverif()
{
	var username;
	username = $("input[name='username']");
	if (username.val() == "")
	{
		username.css({borderColor: "#EF5350"});
		showerror("Username cannot be empty");
		return (0);
	}
	else if (username.val().length < 3 || username.val().length > 16)
	{
		username.css({borderColor: "#EF5350"});
		showerror("Username must be between 3 and 16 characters");
		return (0);
	}
	else
		username.css({borderColor: ""});
	return (1);
}

function passwordverif()
{
	var password;
	password = $("input[name='password']");

	if (password.val() == "")
	{
		password.css({borderColor: "#EF5350"});
		showerror("Password cannot be empty");
		return (0);
	}
	else if (password.val().length < 6 || password.val().length > 32)
	{
		password.css({borderColor: "#EF5350"});
		showerror("Password must be between 6 and 32 characters");
		return (0);
	}
	else
		password.css({borderColor: ""});
	return (1);
}

function passwordmatchverif()
{
	var passverif = $("input[name='passwordverif']");

	if ($("input[name='password']").val() == passverif.val())
	{
		passverif.css({borderColor: ""});
		return (1);
	}
	passverif.css({borderColor: "#EF5350"});
	showerror("The two passwords do not match");
	return (0);
}

function registerinputverif(toggle)
{
	if (toggle == 1)
		verificationtoggle = 1;
	if (verificationtoggle == 1 && (passwordmatchverif() + passwordverif() + emailverif() + usernameverif() + captchaverify()) == 5)
	{
		$("input[type='submit']").prop("disabled", false);
		hideerror();
		verificationtoggle = 0;
	}
	else if (verificationtoggle == 1)
		$("input[type='submit']").prop("disabled", true);
}

function logininputverif(toggle)
{
	if (toggle == 1)
		verificationtoggle = 1;
	if (verificationtoggle == 1 && (passwordverif() + usernameverif()) == 2)
	{
		$("input[type='submit']").prop("disabled", false);
		hideerror();
		verificationtoggle = 0;
	}
	else if (verificationtoggle == 1)
		$("input[type='submit']").prop("disabled", true);
	
}

function showerror(message)
{
	if (message)
	{
		$("#helpMessage").html(message);
		if (showerrorstatus == 0)
		{
				$("#helpMessage").animate({top: ($("#helpMessage").position().top - 40) + "px"}, 100);
				showerrorstatus = 1;
		}
	}
}

function hideerror()
{
	if (showerrorstatus == 1)
	{
		$("#helpMessage").animate({top: ($("#helpMessage").position().top + 40) + "px"}, 100);
		showerrorstatus = 0;
	}
}

function captchaverify()
{
	if (grecaptcha.getResponse())
		return (1)
	showerror("Please check the CAPTCHA")
	setInterval(registerinputverif,1000);
	return (0)
}

function loginerror(type, message)
{
	if (type == "wrong")
	{
		$(".login-form-container .btn-line").css({backgroundColor: "#EF5350"});
		$("#loginMessage").html("Invalid Username or Password");
		$("#loginMessage").animate({opacity: 1}, 100);
		return (1);
	}
	else if (type == "serverError")
	{
		$(".login-form-container .btn-line").css({backgroundColor: "#EF5350"});
		$("#loginMessage").html("Cannot connect to authentication server");
		$("#loginMessage").animate({opacity: 1}, 100);
		return (1);
	}
	else if (type == "correct")
	{
		loginerror("reset");
		$(".login-form-container .btn-line").css({backgroundColor: "#4CAF50"});
		$("#helpMessage").fadeOut();
		$("*").fadeOut(1500);
		return (1);
	}
	else if (type == "custom")
	{
		$(".login-form-container .btn-line").css({backgroundColor: "#EF5350"});
		$("#loginMessage").html(message);
		$("#loginMessage").animate({opacity: 1}, 100);
		return (1);
	}
	else if (type == "reset" && $("#loginMessage").html() != "")
	{
		$(".login-form-container .btn-line").css({backgroundColor: "#607D8B"});
		$("#loginMessage").animate({opacity: 0}, 200);
		$("input[type='text']").val("");
		$("input[type='password']").val("");
	}
	return (0);
}

/*function loginerror(type, message)
{
	if (type == "wrong")
		document.getElementsByClassName("btn-line")[0].style.backgroundColor = "#EF5350";
}*/

function emailverif()
{
	var email;

	email = $("input[name='email']");

	if (email.val() == "")
	{
		email.css({borderColor: "#EF5350"});
		showerror("Email cannot be empty");
		return (0);
	}
	else if (!validateEmail(email.val()))
	{
		email.css({borderColor: "#EF5350"});
		showerror("Email address not valid");
		return (0);
	}
	else
		email.css({borderColor: ""});
	return (1);
}

function forgotpassverif(toggle)
{
	if (toggle == 1)
		verificationtoggle = 1;
	if (verificationtoggle == 1 && emailverif())
	{
		$("input[type='submit']").prop("disabled", false);
		hideerror();
		verificationtoggle = 0;
	}
	else if (verificationtoggle == 1)
		$("input[type='submit']").prop("disabled", true);	
}

function forgotpass()
{
	hideerror();
	$(".login-form-container").children().fadeOut();
	setTimeout(function() {
		$(".login-form-container").children().remove();
		$(".login-form-container").animate({height: "192px"},200);
		setTimeout(function () {
			window.location.replace("/forgot");
		}, 100);
	}, 100);
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
