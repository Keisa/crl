//for normal counters

'use strict';

var elements = document.getElementsByClassName("speed-counter");     //first we get an array of counters
for (var index = 0; index < elements.length; index++)
{
    elements[index].index = -1;
	elements[index].times = 1;
}

setInterval(refresh_counters, 1000);
var i = 0;

function refresh_counters()
{
    var elements = document.getElementsByClassName("speed-counter");
    for (var index = 0; index < elements.length; index++)
    {
        i = elements[index].index
        var info = elements[index].innerHTML.split(' ');
		if (parseInt(info[3]) > parseInt(info[2]) && info[5] == "autosize")
		{
			elements[index].innerHTML = info[0] + " " + info[1] + " " + (info[2] * 2) + " " + info[3] + " " + info[4] + " " + info[5];
        	info = elements[index].innerHTML.split(' ');
			elements[index].times *= 2;
		}
        animate_update(elements[index],Math.round(info[3] / (info[2] - info[1]) * 100));
    }
}


function update_counter(canvas, value)
{
    var info = canvas.innerHTML.split(' ');
    var ctx = canvas.getContext("2d");
    var j = 0;

    ctx.fillStyle="#ffffff";
    ctx.clearRect(0,0,500,300);
    if ((value > 100 && info[5] != "autosize") || value < 0)
	{
    	ctx.font = "50px Rajdhani";
		ctx.fillText("Error in value",120, 170);
        value = 0;
		return (0);
	}
    ctx.beginPath();
    ctx.font = "20px Rajdhani";
	ctx.fillText(info[4],20,40);
	ctx.fillText(canvas.times + "x",460,50);
    ctx.font = "30px Rajdhani";

    while (j <= info[0])
    {
        ctx.fillText((parseInt(info[1]) + Math.round(j * ((info[2] - info[1]) / info[0]))), 230 + 200 * Math.cos((info[0] - j) * Math.PI / info[0]), 220 - 180 * Math.sin(j * Math.PI / info[0]))
        j++;
    }

    ctx.lineWidth=30;
    ctx.strokeStyle="#577e8b";
    ctx.arc(250,220,140, Math.PI, 0);
    ctx.stroke();
    ctx.beginPath();
    ctx.font = "50px Rajdhani";
    ctx.fillText(value + "%", 210, 220);
    ctx.strokeStyle="rgb(" + (value * 2.5) + "," + (208 - value * 2) + ",0)";
    ctx.arc(250,220,140, Math.PI, Math.PI + value * Math.PI / 100);
    ctx.font = "30px Raleway";
    ctx.fillText(canvas.title, 250 - (canvas.title.length / 2) * 15, 270);
    ctx.stroke();
}

function animate_update(element, limit)
{
    temp = Math.round(element.index);
    i = Math.round(i);
    limit = Math.round(limit);
    if (i < limit)
    {
        setTimeout(function() { animate_update(element, limit)}, 20 - (20 * Math.sin((limit - i)/(limit - temp) * 3.14 / 2)));
        i++;
        update_counter(element, i);
    }
    else if (i > limit)
    {
        setTimeout(function() { animate_update(element, limit)}, 20 - (20 * Math.sin((limit - i)/(limit - temp) * 3.14 / 2)));
        i--;
        update_counter(element, i);
    }
    if (i == limit)
        element.index = limit;
}
