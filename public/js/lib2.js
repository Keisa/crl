'use strict';
$(".footer").load("/inc/footer.inc");
$(".navbar").load("/inc/navbar.inc");
$("#ghelp").load("/inc/popup.inc");
$("#telephone").intlTelInput({initialCountry: "MA"});
$("script").remove();
if ($("navbar-transparent"))
{
	$(window).scroll(function() {
		var $height = $(window).scrollTop();
		if($height > 130) {
			$('.navbar').removeClass('navbar-transparent');
		} else {
			$('.navbar').addClass('navbar-transparent');
		}
	});
}

function popup(elementid)
{
	$("#leftradio input").change(function(){
		$('.popup form input:eq(1)').attr("placeholder","Website*");
		$('.popup form input:eq(3)').attr("value","individual");
	});
	$("#rightradio input").change(function(){
		$('.popup form input:eq(1)').attr("placeholder","Company*");
		$('.popup form input:eq(3)').attr("value","business");
	});
	var pop = document.getElementById(elementid)
	var elem = $("#"+elementid)
	if (pop.style.display != "block")
	{
		pop.style.display = "block";
		elem.animate({opacity:1}, 150);
	}
	else
	{
		elem.animate({opacity:0}, 150);
		setTimeout(function(){pop.style.display = "none"},150);
	}	
}

/*function droplist(element)
{
	console.log('droping list again');
	var i = 0;
	element = $(element);
	$("li").each(function (){
		$(this).parent().find(".dropme").animate({opacity: 0, maxHeight: 0}, 100);
	});
	element.parent().find(".dropme").animate({opacity:1, maxHeight: 1000}, 100);
}*/
