function popup(selector, key) {
	let	keyValue = localStorage.getItem(key);
	let	element = $(selector);
	if (keyValue === null)
		element.css('display', 'block');
	$(selector + ' .btn-close').click(function () {
		element.css('display', 'none');
		localStorage.setItem(key, true);
	});
}
