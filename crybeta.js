'use strict';
const express = require('express');
const exphbs = require('express-handlebars');
const expressLayouts = require('express-ejs-layouts');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session')
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const Swal = require('sweetalert2');
const morgan = require('morgan');
const path = require('path');
const helmet = require('helmet');

const app = express();

// Forcing traffic to go over HTTPS
app.use(helmet());

// Express session.
app.use(
  session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
	})
);
// Connect flash (Temporary, this will be changed to SWAL later on)
app.use(flash());

// Passport Config
require('./config/passport')(passport);
require('./routes/redirects')(app, __dirname);

// DB Config
const db = require('./config/keys').mongoURI;

// Connecting to Mongo
mongoose.set('useCreateIndex', true) // Fixing the ensureindex deprecated issue.
mongoose.connect(
	db,
	{ useNewUrlParser: true }
	)
	.then(() => console.log('Connected to Database successfully.'))
	.catch(err => console.log(err));

// EJS parsing
app.use(expressLayouts);
app.set('view engine', 'ejs');

// Logging to console on development env.
app.use(morgan(':req[content-type] -> :res[content-type]'));

app.use(bodyParser.json());
app.use(cookieParser());

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash (Temporary, this will be changed to SWAL later on)
//app.use(flash());

// Global variables
app.use(function(req, res, next) {
 res.locals.success_msg = req.flash('success_msg');
 res.locals.error_msg = req.flash('error_msg');
 res.locals.error = req.flash('error');
 next();
});

// Routes
app.use('/login', require('./routes/index.js'));
app.use('/users', require('./routes/users.js'));

const PORT = process.env.PORT || 1337;

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}!`);
});
