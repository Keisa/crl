module.exports = {
  ensureAuthenticated: function(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    swal("Oops!", "You need to sign in first.", "error");
    res.redirect('/login');
  }
};
